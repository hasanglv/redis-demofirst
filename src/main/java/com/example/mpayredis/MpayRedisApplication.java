package com.example.mpayredis;

import com.example.mpayredis.service.HashRedisTemplateExample;
import com.example.mpayredis.service.ListRedisExample;
import com.example.mpayredis.service.ListRedisTemplateExample;
import com.example.mpayredis.service.StringRedisTemplateExample;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class MpayRedisApplication implements CommandLineRunner {

    private final StringRedisTemplateExample stringRedisExample;
    private final ListRedisTemplateExample listRedisTemplateExample;
    private final HashRedisTemplateExample hashRedisTemplateExample;

    public static void main(String[] args) {

        SpringApplication.run(MpayRedisApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        stringRedisExample.run();
//        ListRedisExample listRedisExample = new ListRedisExample();
//        listRedisExample.run();
//        listRedisTemplateExample.run();
        hashRedisTemplateExample.run();



    }
}
