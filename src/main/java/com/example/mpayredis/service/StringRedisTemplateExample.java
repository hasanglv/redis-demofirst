package com.example.mpayredis.service;


import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class StringRedisTemplateExample {


    private final RedisTemplate<String, String> redisTemplate;

    public void run() {
        // Setting a value
        redisTemplate.opsForValue().set("bike:1", "Deimos");
        String res1 = redisTemplate.opsForValue().get("bike:1");
        System.out.println(res1); // Deimos

        // Setting a value only if key does not exist
        Boolean res2 = redisTemplate.opsForValue().setIfAbsent("bike:1", "bike");
        System.out.println(res2); // false (because key already exists)
        System.out.println(redisTemplate.opsForValue().get("bike:1")); // Deimos (value is unchanged)

        // Setting a value only if key exists
        Boolean res3 = redisTemplate.opsForValue().setIfPresent("bike:1", "hasan");
        System.out.println(res3); // true
        System.out.println(redisTemplate.opsForValue().get("bike:1")); // hasan

        // Setting multiple values
        redisTemplate.opsForValue().multiSet(Map.of(
                "bike:1", "Deimos",
                "bike:2", "Ares",
                "bike:3", "Vanth"
        ));
        List<String> res4 = redisTemplate.opsForValue().multiGet(List.of("bike:1", "bike:2", "bike:3"));
        System.out.println(res4); // [Deimos, Ares, Vanth]

        // Incrementing a value
        redisTemplate.opsForValue().set("total_crashes", "0");
        Long res5 = redisTemplate.opsForValue().increment("total_crashes");
        System.out.println(res5); // 1
        Long res6 = redisTemplate.opsForValue().increment("total_crashes", 10);
        System.out.println(res6); // 11
    }

}