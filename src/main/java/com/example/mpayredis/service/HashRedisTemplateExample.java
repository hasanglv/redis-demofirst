package com.example.mpayredis.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class HashRedisTemplateExample {

    private final RedisTemplate<String,String> redisTemplate;

    public void run() {
        try {
            HashOperations<String, String, String> hashOps = redisTemplate.opsForHash();

            Map<String, String> bike1 = new HashMap<>();
            bike1.put("model", "Deimos");
            bike1.put("brand", "Ergonom");
            bike1.put("type", "Enduro bikes");
            bike1.put("price", "4972");

            hashOps.putAll("bike:1", bike1);

            String res2 = hashOps.get("bike:1", "model");
            System.out.println(res2); // Deimos

            String res3 = hashOps.get("bike:1", "price");
            System.out.println(res3); // 4972

            Map<String, String> res4 = hashOps.entries("bike:1");
            System.out.println(res4); // {type=Enduro bikes, brand=Ergonom, price=4972, model=Deimos}

            List<String> res5 = hashOps.multiGet("bike:1", List.of("model", "price"));
            System.out.println(res5); // [Deimos, 4972]

            Long res6 = hashOps.increment("bike:1", "price", 100);
            System.out.println(res6); // 5072
            Long res7 = hashOps.increment("bike:1", "price", -100);
            System.out.println(res7); // 4972

            Long res8 = hashOps.increment("bike:1:stats", "rides", 1);
            System.out.println(res8); // 1
            Long res9 = hashOps.increment("bike:1:stats", "rides", 1);
            System.out.println(res9); // 2
            Long res10 = hashOps.increment("bike:1:stats", "rides", 1);
            System.out.println(res10); // 3
            Long res11 = hashOps.increment("bike:1:stats", "crashes", 1);
            System.out.println(res11); // 1
            Long res12 = hashOps.increment("bike:1:stats", "owners", 1);
            System.out.println(res12); // 1
            String res13 = hashOps.get("bike:1:stats", "rides");
            System.out.println(res13); // 3
            List<String> res14 = hashOps.multiGet("bike:1:stats", List.of("crashes", "owners"));
            Map<String, String> res15 = hashOps.entries("bike:1:stats");
            System.out.println(res15);

            System.out.println(res14); // [1, 1]
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}