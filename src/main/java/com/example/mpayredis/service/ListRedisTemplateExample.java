package com.example.mpayredis.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.connection.DataType;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class ListRedisTemplateExample {

    private final RedisTemplate<String, String> redisTemplate;


    public void run() {
        // Queue operations
        Long res1 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:1");
        System.out.println(res1);  // >>> 1

        Long res2 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:2");
        System.out.println(res2);  // >>> 2

        String res3 = redisTemplate.opsForList().rightPop("bikes:repairs");
        System.out.println(res3);  // >>> bike:1

        String res4 = redisTemplate.opsForList().rightPop("bikes:repairs");
        System.out.println(res4); // >>> bike:2

        // Stack operations
        Long res5 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:1");
        System.out.println(res5);  // >>> 1

        Long res6 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:2");
        System.out.println(res6);  // >>> 2

        String res7 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res7);  // >>> bike:2

        String res8 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res8);  // >>> bike:1

        Long res9 = redisTemplate.opsForList().size("bikes:repairs");
        System.out.println(res9);  // >>> 0

        Long res10 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:1");
        System.out.println(res10);  // >>> 1

        Long res11 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:2");
        System.out.println(res11);  // >>> 2

        String res12 = redisTemplate.opsForList().rightPopAndLeftPush("bikes:repairs", "bikes:finished");
        System.out.println(res12);  // >>> bike:2

        List<String> res13 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res13);  // >>> [bike:1]

        List<String> res14 = redisTemplate.opsForList().range("bikes:finished", 0, -1);
        System.out.println(res14);  // >>> [bike:2]

        Long res15 = redisTemplate.opsForList().rightPush("bikes:repairs", "bike:1");
        System.out.println(res15);  // >>> 1

        Long res16 = redisTemplate.opsForList().rightPush("bikes:repairs", "bike:2");
        System.out.println(res16);  // >>> 2

        Long res17 = redisTemplate.opsForList().leftPush("bikes:repairs", "bike:important_bike");
        System.out.println(res17);  // >>> 3

        List<String> res18 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res18);  // >>> [bike:important_bike, bike:1, bike:2]

        Long res19 = redisTemplate.opsForList().rightPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3");
        System.out.println(res19);  // >>> 3

        Long res20 = redisTemplate.opsForList().leftPushAll("bikes:repairs", "bike:important_bike", "bike:very_important_bike");
        System.out.println(res20);  // >>> 5

        List<String> res21 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res21);  // >>> [bike:very_important_bike, bike:important_bike, bike:1, bike:2, bike:3]

        Long res22 = redisTemplate.opsForList().rightPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3");
        System.out.println(res22);  // >>> 3

        String res23 = redisTemplate.opsForList().rightPop("bikes:repairs");
        System.out.println(res23);  // >>> bike:3

        String res24 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res24);  // >>> bike:1

        String res25 = redisTemplate.opsForList().rightPop("bikes:repairs");
        System.out.println(res25);  // >>> bike:2

        String res26 = redisTemplate.opsForList().rightPop("bikes:repairs");
        System.out.println(res26);  // >>> null

        Long res27 = redisTemplate.opsForList().leftPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3", "bike:4", "bike:5");
        System.out.println(res27);  // >>> 5

        redisTemplate.opsForList().trim("bikes:repairs", 0, 2);
        List<String> res28 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res28);  // >>> [bike:5, bike:4, bike:3]

        res27 = redisTemplate.opsForList().rightPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3", "bike:4", "bike:5");
        System.out.println(res27);  // >>> 5

        redisTemplate.opsForList().trim("bikes:repairs", -3, -1);
        res28 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res28);  // >>> [bike:3, bike:4, bike:5]

        Long res31 = redisTemplate.opsForList().rightPushAll("bikes:repairs", "bike:1", "bike:2");
        System.out.println(res31);  // >>> 2

        String res32 = redisTemplate.opsForList().rightPop("bikes:repairs", 1, TimeUnit.SECONDS);
        System.out.println(res32);  // >>> bike:2

        String res33 = redisTemplate.opsForList().rightPop("bikes:repairs", 1, TimeUnit.SECONDS);
        System.out.println(res33);  // >>> bike:1

        String res34 = redisTemplate.opsForList().rightPop("bikes:repairs", 1, TimeUnit.SECONDS);
        System.out.println(res34);  // >>> null

        Boolean res35 = redisTemplate.delete("new_bikes");
        System.out.println(res35);  // >>> 0

        Long res36 = redisTemplate.opsForList().leftPushAll("new_bikes", "bike:1", "bike:2", "bike:3");
        System.out.println(res36);  // >>> 3

        redisTemplate.opsForValue().set("new_bikes", "bike:1");
        String res37 = redisTemplate.opsForValue().get("new_bikes");
        System.out.println(res37);  // >>> bike:1

        DataType res38 = redisTemplate.type("new_bikes");
        System.out.println(res38);  // >>> string

        try {
            Long res39 = redisTemplate.opsForList().leftPushAll("new_bikes", "bike:2", "bike:3");
        } catch (Exception e) {
            e.printStackTrace();
            // >>> redis.clients.jedis.exceptions.JedisDataException:
            // >>> WRONGTYPE Operation against a key holding the wrong kind of value
        }

        redisTemplate.opsForList().leftPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3");
        Boolean res40 = redisTemplate.hasKey("bikes:repairs");
        System.out.println(res40);  // >>> true

        String res41 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res41);  // >>> bike:3

        String res42 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res42);  // >>> bike:2

        String res43 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res43);  // >>> bike:1

        Boolean res44 = redisTemplate.hasKey("bikes:repairs");
        System.out.println(res44);  // >>> false

        Boolean res45 = redisTemplate.delete("bikes:repairs");
        System.out.println(res45);  // >>> 0

        Long res46 = redisTemplate.opsForList().size("bikes:repairs");
        System.out.println(res46);  // >>> 0

        String res47 = redisTemplate.opsForList().leftPop("bikes:repairs");
        System.out.println(res47);  // >>> null

        Long res48 = redisTemplate.opsForList().leftPushAll("bikes:repairs", "bike:1", "bike:2", "bike:3", "bike:4", "bike:5");
        System.out.println(res48);  // >>> 5

        redisTemplate.opsForList().trim("bikes:repairs", 0, 2);
        List<String> res49 = redisTemplate.opsForList().range("bikes:repairs", 0, -1);
        System.out.println(res49);  // >>> [bike:5, bike:4, bike:3]

    }
}
