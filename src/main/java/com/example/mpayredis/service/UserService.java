package com.example.mpayredis.service;

import com.example.mpayredis.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.UnifiedJedis;
import redis.clients.jedis.params.SetParams;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final RedisTemplate<String, String> redisTemplate;


    private static final String USER_KEY_PREFIX = "USER_";


//    public void saveUser(User user) {
//        redisTemplate.opsForValue().set(USER_KEY_PREFIX + user.getId(), user);
//    }

//    public User getUserById(Integer id) {
//        return (User) redisTemplate.opsForValue().get(USER_KEY_PREFIX + id);
//    }
//
//    public void deleteUserById(Integer id) {
//        redisTemplate.delete(USER_KEY_PREFIX + id);
//    }


}
